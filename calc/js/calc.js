
//creates object called Calculator for storing data needed for calculator expression
//displayValue stores string for displaying to user
//firstOperand stores the number input by user
//Waiting will check if a secondOperand needs to be used for the expression
//operator stores the operator value
const calculator = {
  displayValue: '0',
  firstOperand: null,
  waitingForSecondOperand: false,
  operator: null,
};

//updateDisplay finds the Calculator-screen and sets it to be the displat value
function updateDisplay() {
  const display = document.querySelector('.calculator-screen');
  display.value = calculator.displayValue;
}


updateDisplay();

//New object keys defined as the keys in HTML
//Add event listener for Keys object
//Target object set to equal the event listener
const keys = document.querySelector('.calculator-keys');
keys.addEventListener('click', (event)=>{
  const {target} = event;

  //If target is not a button then exit function
  if(!target.matches('button')){
    return;
  }

  //If target clicked is operator then call HandleOperator function
  //updateDisplay
  if(target.classList.contains('operator')){
    handleOperator(target.value);
    updateDisplay();
    return;
  }

  //If target class clicked is 'decimal' then call inputDecimal function
  //updateDisplay
  if(target.classList.contains('decimal')){
    inputDecimal(target.value);
		updateDisplay();
    return;
  }

  //If taget class clicked is 'all-clear' then call resetCalculator function
  //updateDisplay
  if(target.classList.contains('all-clear')){
    resetCalculator();
    updateDisplay();
    return;
  }

  //call inputDigit function to add numbers to calculator
  inputDigit(target.value);
  updateDisplay();
})


function inputDigit(digit){
  const{displayValue, waitingForSecondOperand} =calculator;

  //if waitingForSecondOperand is true then display value equals the key that was clicked
  // waitingForSecondOperand goes back to being false so another key can be clicked
  if(waitingForSecondOperand === true){
    calculator.displayValue = digit;
    calculator.waitingForSecondOperand =false;
  }else {
    calculator.displayValue =displayValue === '0'? digit :displayValue +digit;
  }

  console.log(calculator);
}


function inputDecimal(dot){
  //If displayValue does not have 'dot' then append 'dot' to displayValue
  if(!calculator.displayValue.includes(dot)){
    calculator.displayValue +=dot;
  }
}


function handleOperator(nextOperator){
  const {firstOperand, displayValue, operator} = calculator

  //current number value (currently a string) on screen is conversted to a integer here
  const inputValue = parseFloat(displayValue);

  //operator is present and waitingForSecondOperand is true
  //then update operator with the new one user has clicked
  if (operator && calculator.waitingForSecondOperand) {
    calculator.operator =nextOperator;
    console.log(calculator);
    return;
  }

  //if firstOperand is null then firstOperand equals the inputValue
  //Else if operator clicked then performCalculation and convert to a string
  //for the result to be displayed
  if(firstOperand === null){
    calculator.firstOperand = inputValue;
  }else if(operator){
    const result = performCalculation[operator](firstOperand, inputValue);
    calculator.displayValue = String(result);
    calculator.firstOperand =result;
  }
  calculator.waitingForSecondOperand =true;
  calculator.operator = nextOperator;
  console.log(calculator);
}

//Object for performing calculation useing first and second operands
const performCalculation = {
  '/':(firstOperand, secondOperand) => firstOperand / secondOperand,
  '*':(firstOperand, secondOperand) => firstOperand * secondOperand,
  '+':(firstOperand, secondOperand) => firstOperand + secondOperand,
  '-':(firstOperand, secondOperand) => firstOperand - secondOperand,

  //SQR and SQ calculations only work partically
  //Users have to click second secondOperand in order to get the value for them
  'SQR':(firstOperand, secondOperand) => Math.sqrt(firstOperand),
  'SQ':(firstOperand) => Math.pow(firstOperand, 2),

  '=':(firstOperand, secondOperand) => firstOperand
};

//Function resets calculator to be the the standard that is starts out at.
function resetCalculator(){
  calculator.displayValue = '0';
  calculator.firstOperand = null;
  calculator.waitingForSecondOperand = false;
  calculator.operator =null;
  console.log(calculator);
}
